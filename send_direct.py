from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.common.exceptions
import time
import random
from data_handler import *
import clipboard





def access_user_direct(username, chrome):
    new_message = chrome.find_element(by=By.CLASS_NAME, value="_aa4m._aa4p")
    new_message.click()
    time.sleep(random.uniform(0.9, 1.2))
    search_in_direct_message = chrome.find_element(by=By.CLASS_NAME, value="_aaie._aaid._aaiq")
    search_in_direct_message.send_keys(username)
    time.sleep(random.uniform(1.1, 2.9))
    search_result = chrome.find_elements(by=By.CLASS_NAME, value="_abm4")
    for result in search_result:
        try:
            if result.text.split("\n")[0] == username:
                result.click()
                time.sleep(0.5)
                break
        except:
            pass
    try:
        next_button = chrome.find_element(by=By.CLASS_NAME, value="_acan._acao._acas._acav")
        next_button.click()
        return True
    except:
        return False




def send_message(message, chrome):
    try:
        copy_msg = clipboard.copy(message)
        message_box = chrome.find_element(by=By.TAG_NAME, value="textarea")
        message_box.send_keys(Keys.SHIFT, Keys.INSERT)
        time.sleep(random.uniform(0.5, 1))
        message_box.send_keys(Keys.RETURN)
        return True
    except Exception as err:
        print(err)
        return False



def send_direct(username, message, chrome):
    chrome.get("https://www.instagram.com/direct/inbox/")
    time.sleep(random.uniform(2.5, 4.9))
    if access_user_direct(username, chrome):
        time.sleep(random.uniform(3.1, 5.1))
        if not send_message(message, chrome):
            print(f"Could not send message --> {username}")
    else:
        print(f"user not found --> {username}")




def sender(username_list, chrome, message_to_send):
    time.sleep(random.randint(2, 8))
    for user in username_list:
        send_direct(user, message_to_send, chrome)
        add_to_sent(user)
        time.sleep(random.randint(1, 3))

    # close webdriver
    # chrome.close()


# main()