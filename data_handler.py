def add_to_csv(usernames, write_mode):
    if write_mode == "a":
        usernames = f"{usernames}"
    with open("Datas/unsend.csv", write_mode) as infile:
        infile.write(usernames)

# add_to_csv("userA_,\nuserB", "a")





def check_for_sent_direct(username, data):
    for old_username in data:
        if username == old_username:
            return False
    return True

# print(check_for_sent_direct("alii", "emad\nali\nmamad"))





def read_unsend():
    usernames_list = []
    with open("Datas/unsend.csv", "r") as infile:
        usernames = infile.read()
        for username in usernames.split("\n"):
            if len(username) != 0:
                usernames_list.append(username)
    return usernames_list

# print(read_unsend())




def read_sent():
        usernames_list = []
        with open("Datas/sent.csv", "r") as infile:
            usernames = infile.read()
            for username in usernames.split("\n"):
                if len(username) != 0:
                    usernames_list.append(username)
        return usernames_list

# print(read_sent())






def add_to_sent(username):
    with open("Datas/sent.csv", "a") as infile:
        infile.write(f"{username}\n")

# add_to_sent("farbod")   





def read_msg():
    with open("Datas/message.txt", "r") as infile:
        return infile.read()

# print(read_msg())