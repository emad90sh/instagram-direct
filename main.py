from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import selenium.common.exceptions
import time
import random
from data_handler import *
from send_direct import *
import pickle as pl
import sys


target_user = "limoo.me"
get_number = 5





def scrol_to_load_more():
    followers_list  = chrome.find_element(by=By.XPATH, value="//div[@class='_aano']")
    for _ in range(20):
        chrome.execute_script('arguments[0].scrollTop = arguments[0].scrollTop + arguments[0].offsetHeight;', followers_list)
        time.sleep(random.randint(1, 2))



def gather_user():
    global counter
    all_followers = chrome.find_elements(by=By.CLASS_NAME, value="_ab8y._ab94._ab97._ab9f._ab9k._ab9p._abcm")
    usernames_csv_format = ""
    for follower in all_followers:
        if counter == get_number:
            break
        username = follower.text
        if check_for_sent_direct(username, sent_users):
            usernames_csv_format += f"{username}\n"
            counter += 1
    add_to_csv(usernames_csv_format, "a")






def main(relogin):
    global chrome, sent_users, counter

    sent_users = read_sent()

    counter = 0

    add_to_csv("", "w")

    options = webdriver.ChromeOptions()
    options.add_argument('-user-data-dir=../chrome_dir')
    driver_path = "../chromedriver"

    # run chrome as chromedriver
    chrome = webdriver.Chrome(driver_path, options=options)

    # open url
    chrome.get("https://instagram.com")
    cookies = pl.load(open("../cookies.pl", "rb"))
    for cookie in cookies:
        chrome.add_cookie(cookie)
    time.sleep(random.randint(1, 3))
    chrome.get("https://instagram.com")
    time.sleep(random.randint(1, 3))

    if relogin:
        input("Login then clicke to continue")
        pl.dump(chrome.get_cookies() , open("../cookies.pl","wb"))

    try:
        # chrome.find_element(by=By.CLASS_NAME, value="username")
        chrome.find_element(by=By.XPATH, value="//input[@name='username']")
        input("Login then clicke to continue")
        pl.dump(chrome.get_cookies() , open("../cookies.pl","wb"))
    except:
        pass

    try:
        notification = chrome.find_elements(by=By.XPATH, value="//div[@class='_a9-z']//button")
        notification[1].click()
        time.sleep(0.1)
        print("Notification disabled")
    except:print("No notification founded")

    # open url
    chrome.get(f"https://instagram.com/{target_user}/followers")
    time.sleep(random.randint(10, 11))

    while True:
        if counter >= get_number:
            break
        scrol_to_load_more()
        time.sleep(random.uniform(1.1, 3.5))
        gather_user()
        time.sleep(1)

    time.sleep(2)

    send_users = read_unsend()
    message = read_msg()
    sender(send_users, chrome, message)


    pl.dump(chrome.get_cookies() , open("../cookies.pl","wb"))
    # close webdriver
    chrome.close()


main(relogin=True)